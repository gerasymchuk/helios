const soap = require('soap');

const TAG = '[helios-operation]';
var FUNCTION_NAMES = {
    '10': 'Configuration',
    '15': 'Execute  Script',
    '51': 'Operational Mode Change',
    '52': 'Reset',
    '53': 'Reconciliate',
    '200': 'Message',
    '301': 'Input Change',
    '302': 'Output Change',
    '500': 'Support',
    '900': 'Statistics',
    '60': 'Policy Change',
    '65': 'Mode Change',
    '66': 'Schedule Change',
    '1001': 'Entry',
    '1002': 'Cancelled Entry',
    '1004': 'Allow Entry',
    '1101': 'Temperature Change',
    '1201': 'Cash Deposit',
    '1202': 'Cash Withdrawal',
    '1203': 'Emptying',
    '1204': 'Refill',
    '1205': 'Balance Update',
    '1206': 'Cash Movement',
    '1301': 'Movement',
    '1302': 'Start Recording',
    '1303': 'Stop Recording',
    '1304': 'Activate  Surveillance',
    '1305': 'Deactivate Surveillance',
    '1306': 'Retrieve Recording',
    '1307': 'Take Snapshot',
    '1308': 'Alarm',
    '1309': 'Cancel Alarm',
    '2501': 'Start Playback',
    '2502': 'Playback Started',
    '2503': 'Screen Overlay',
    '3001': 'Weather Change'
};


module.exports = function (RED) {

    function HeliosOperationNode(config) {
        RED.nodes.createNode(this, config);
        //this.policy_params = config.policy_params;
        var node = this;

        this.on('input', function (msg) {
            node.status({});
            console.log("type: " + config.operation_type);
            console.log("policies count: " + config.policy_params.length);
            console.log("function params count: " + config.function_params.length);
            console.log("event params count: " + config.event_params.length);

            node.payload = {};
            node.payload.functions = msg.payload.functions || [];
            node.payload.policies = msg.payload.policies || [];
            node.payload.events = msg.payload.events || [];

            switch (config.operation_type) {
                case "event":
                    if(config.event_type) {
                        console.log("event: " + config.event_type);
                        node.payload.events.push({
                            Id: config.event_type,
                            ParameterList: {Parameter: config.event_params}
                        });
                    }
                    break;
                case "function":
                    if(config.function_type) {
                        console.log("function: " + FUNCTION_NAMES[config.function_type.toString()]);
                        var p = [];
                        for(var i = 0; i < config.function_params.length; i++) {
                            p.push(config.function_params[i]);
                        }
                        console.log("params: " + JSON.stringify(p));
                        node.payload.functions.push({
                            Name: FUNCTION_NAMES[config.function_type.toString()],
                            ParameterList: {Parameter: p},
                            TypeId: config.function_type
                        });
                    }
                    break;
                case "policy":
                    for (var i = 0; i < config.policy_params.length; i++) {
                        console.log("policy: " + config.policy_params[i].Name + " = " + config.policy_params[i].Value + " Details: " + config.policy_params[i].Details);
                        node.payload.policies.push(config.policy_params[i]);
                    }
                    break;
            }

            node.status({fill:"green",shape:"dot",text:"connected"});
            console.log("Payload: " + JSON.stringify(node.payload));
            node.send({payload: node.payload});
        });
    }

    RED.nodes.registerType("Operation", HeliosOperationNode);
};