const request = require('request');
const builder = require('xmlbuilder');
const dateFormat = require('dateformat');
const TAG = '[helios-sms]';

module.exports = function (RED) {
    function HeliosSMS(config) {
        RED.nodes.createNode(this, config);

        const node = this;


        this.on('input', function (msg) {
            node.status({});
            var data = {};
            if (config.data_source == 'payload') {
                // Sample:
                // {
                //     "Endpoint": "",
                //     "Company": "",
                //     "Username": "",
                //     "Password": "",
                //     "Number": "",
                //     "Message": "",
                // }
                if (msg.payload) {
                    data = msg.payload;
                } else {
                    return;
                }
            } else {
                data = {
                    Endpoint: config.url,
                    Company: config.company,
                    Username: config.username,
                    Password: config.password,
                    Number: config.number,
                    Message: config.message
                };
            }
            console.log("Payload: " + JSON.stringify(data));
            const now = new Date();
            const root = builder.create('sms-teknik');
            root.ele('operationtype', 0);
            root.ele('flash', 0);
            root.ele('multisms', 0);
            root.ele('ttl', 0);
            root.ele('customid', '');
            root.ele('compresstext', 0);
            root.ele('send_date', dateFormat(now, "isoDate"));
            root.ele('send_time', dateFormat(now, "isoTime"));
            root.ele('udh', '');
            root.ele('udmessage').dat(data.Message);
            root.ele('smssender', data.Company);
            root.ele('deliverystatustype', 0);
            root.ele('deliverystatusaddress', '');
            root.ele('usereplynumber', 0);
            root.ele('usereplyforwardtype', 0);
            root.ele('usereplyforwardurl', '');
            root.ele('usereplycustomid', '');
            root.ele('usee164', 0);
            root.ele('items').ele('recipient').ele('nr', data.Number);
            const body = root.end({ pretty: true});

            const sendSMSUrl = data.Endpoint + '/send/xml/' + "?id=" + data.Company.replace(/ /g, "+") + "&user=" + data.Username + "&pass=" + data.Password;
            console.log(TAG + " url: " + sendSMSUrl);
            console.log(TAG + "body: \n" + body);
            request(
                {
                    method: 'POST',
                    uri: 'https://' + sendSMSUrl,
                    headers: {
                        'Content-Type': 'text/xml'
                    },
                    body: body.toString()
                }
                , function (error, response, body) {
                    console.log("Response code: " + response.statusCode);
                    if (!error && response.statusCode == 200) {
                        console.log(TAG + " Sucess: " + body);
                        node.send({payload: body});
                    } else if (error) {
                        console.log(TAG + " Error: " + error);
                        node.error(error);
                    }
                });
        });
    }

    RED.nodes.registerType("SMS", HeliosSMS);
};
