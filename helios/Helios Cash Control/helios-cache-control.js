const soap = require('soap');
const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

// const WSDL = require('soap/lib/wsdl').WSDL;
// const orig = WSDL.prototype.objectToXML;
// WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
//     var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]);
//     if(xml.indexOf("SetPolicy") > -1) {
//         xml = orderAlphabetically(xml, 'policy');
//     }
//     return xml;
// };
//
// function orderAlphabetically(xml, nodeName) {
//     var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
//     var p = d.getElementsByTagName(nodeName);
//     Array.prototype.slice.call(p).forEach(function (element, index, array) {
//         Array.prototype.slice.call(element.childNodes)
//             .sort(function (x, y) {
//                 if(x.nodeName < y.nodeName) return -1;
//                 if(x.nodeName > y.nodeName) return 1;
//                 return 0;
//             }).forEach(function (x) { element.appendChild(x); });
//     });
//     return xmlserializer.serializeToString(d);
// }

const COMMAND_SOAP_PATH = '/CommandService.svc';
const EXECUTE_COMMAND_ACTION = 'urn:executeCommand';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-cash-control]';

function executeCommand(url, data, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.on('request', function (xmlMsg) {
                    console.log("setPolicy XML: " + xmlMsg);
                });
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, EXECUTE_COMMAND_ACTION);
                client.executeCommand(data, function (err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else {
                        resolve(result);
                    }
                });
            }
        }, url);
}

module.exports = function (RED) {
    function HeliosCashControlNode(config) {
        RED.nodes.createNode(this, config);

        this.data_source = config.data_source;

        const node = this;

        this.on('input', function (msg) {
            node.status({});

            node.msg = msg || {};
            node.payload = (msg && msg.payload) || {};

            if (node.data_source == 'payload') {
                if (!msg || !msg.payload) {
                    throw new Error('Input Error: payload is empty');
                }
            } else {
                node.payload = {
                    locationId: config.location_id,
                    deviceId: config.device_id,
                    applicationId: config.application_id,
                    command: config.command
                };
            }

            console.log("source: " + node.data_source);
            console.log("payload: " + JSON.stringify(node.payload));

            executeCommand(config.url + COMMAND_SOAP_PATH, node.payload, onResult, onEerror);
        });

        function onResult(result) {
            if (result) {
                console.log(TAG, 'result', result);
                node.status({fill:"green",shape:"dot",text:"executed"});
            }
        }

        function onEerror(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Cash Control", HeliosCashControlNode);
};