const soap = require('soap');
const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

const WSDL = require('soap/lib/wsdl').WSDL;
const orig = WSDL.prototype.objectToXML;
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]);
    if(xml.indexOf("SetPolicy") > -1) {
        xml = orderAlphabetically(xml, 'policy');
    }
    return xml;
};

function orderAlphabetically(xml, nodeName) {
    var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
    var p = d.getElementsByTagName(nodeName);
    Array.prototype.slice.call(p).forEach(function (element, index, array) {
        Array.prototype.slice.call(element.childNodes)
            .sort(function (x, y) {
                if(x.nodeName < y.nodeName) return -1;
                if(x.nodeName > y.nodeName) return 1;
                return 0;
            }).forEach(function (x) { element.appendChild(x); });
    });
    return xmlserializer.serializeToString(d);
}

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const POLICY_SOAP_PATH = '/PolicyService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const SET_POLICY_ACTION = 'http://tempuri.org/IPolicyService/SetPolicy';
const GET_POLICY_ACTION = 'http://tempuri.org/IPolicyService/GetPolicy';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-device-policy]';

function authorize(url, args, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
                client.Authorize(args, function (err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                        resolve({
                            token: result.AuthorizeResult.AuthenticationToken,
                            device_id: result.AuthorizeResult.EntityId
                        });
                    }
                });
            }
        }, url);
}

function getPolicy(url, args, results, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                // client.on('request', function (xmlMsg) {
                //     console.log("getPolicy XML: " + xmlMsg);
                // });
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_POLICY_ACTION);
                client.GetPolicy(args.shift(), function (err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else {
                        resolve(url, args, results.concat(result));
                    }
                });
            }
        }, url);
}

function setPolicy(url, args, results, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                // client.on('request', function (xmlMsg) {
                //     console.log("setPolicy XML: " + xmlMsg);
                // });
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SET_POLICY_ACTION);
                client.SetPolicy(args.shift(), function (err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else {
                        resolve(url, args, results.concat(result));
                    }
                });
            }
        }, url);
}

module.exports = function (RED) {

    function HeliosPolicyNode(config) {
        RED.nodes.createNode(this, config);

        const authConfig = RED.nodes.getNode(config.auth);

        this.url = config.url;

        this.policy_data_source = config.policy_data_source;
        this.policy_direction = config.policy_direction;
        this.policy_params = config.policy_params;
        const node = this;

        this.on('input', function (msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});

            node.msg = msg || {};
            node.payload = (msg && msg.payload) || {};

            if (node.policy_data_source == 'payload') {
                if (msg && msg.payload) {
                    node.policy_direction = (msg.payload.PolicyDirection && msg.payload.PolicyDirection.toLowerCase() == "get") ? "get" : "set";
                    node.policy_params = msg.payload.PolicyList;
                    node.device_id = msg.payload.EntityId;

                    delete node.payload.PolicyDirection;
                    delete node.payload.PolicyList;
                    delete node.payload.EntityId;
                } else {
                    throw new Error('Input Error: payload is empty');
                }
            }

            console.log("source: " + node.policy_data_source);
            console.log("policy_direction: " + node.policy_direction);
            console.log("policies count: " + node.policy_params.length);

            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error );
        });

        function onAuthorized(credentials) {
            console.log("Auth:" + JSON.stringify(credentials));
            node.credentials = credentials;
            node.status({fill:"green",shape:"dot",text:"connected"});
            const p = node.policy_params.map(function(obj){
                console.log("param: " + obj.Name + " = " + obj.Value + " Details: " + obj.Details);
                return node.policy_direction == "set" ? {
                    authenticationToken: credentials.token,
                    entityId: node.device_id || credentials.device_id,
                    policy: obj
                } : {
                    authenticationToken: credentials.token,
                    entityId: node.device_id || credentials.device_id,
                    policyName: obj.Name
                };
            });
            policyRequest(config.url + POLICY_SOAP_PATH, p, []);
        }

        function policyRequest(url, data, result) {
            if(data && data.length) {
                node.policy_direction == "set"
                    ? setPolicy(url, data, result, policyRequest, error)
                    : getPolicy(url, data, result, policyRequest, error)
            } else {
                if (result && node.policy_direction == "get") {
                    console.log(TAG, 'result', result);
                    node.payload.EntityId = node.credentials.device_id;
                    node.payload.PolicyList = result.map(function (value) {
                        return value.GetPolicyResult;
                    });
                }
                node.msg.payload = node.payload;
                node.send(node.msg);
            }
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Policy", HeliosPolicyNode);
};