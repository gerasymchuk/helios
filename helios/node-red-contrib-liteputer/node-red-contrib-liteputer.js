module.exports = function (RED) {
    function litePuterNode(config) {
        RED.nodes.createNode(this, config);
        const node = this;

        function getValue(name, s) {
            return s.hasOwnProperty(name) ? s[name] : -1;
        }

        function removeChannelByIndex(arr, index) {
            for(var i = 0; i < arr.length; i++) {
                if(arr[i].channel === index) {
                    arr.splice(i, 1);
                }
            }
        }

        function checkValue(v) {
            return v > -1 && v < 256;
        }

        function pushValue(arr, index, v) {
            if(checkValue(v)) {
                removeChannelByIndex(arr, index);
                arr.push({channel:index, value:v});
                return v;
            }
            return -1;
        }

        function parse(source, buckets) {
            var buckets = buckets || [];
            var index = source.address_start || 1;

            pushValue(buckets, index++, getValue("channel1", source));
            pushValue(buckets, index++, getValue("channel2", source));
            pushValue(buckets, index++, getValue("channel3", source));

            if(source.device_type == "LDX-405A") {
                pushValue(buckets, index++, getValue("channel4", source));
            }

            pushValue(buckets, index++, getValue("mode", source));
            pushValue(buckets, index++, getValue("chaser_speed", source));
            pushValue(buckets, index++, getValue("master", source));

            return buckets;
        }

        this.on('input', function (msg) {
            var fromPayload = config.data_source === 'payload';
            var source = fromPayload ? msg.payload : config;
            node.send({payload: {buckets: parse(source, msg.payload.buckets)}});
        });
    }

    RED.nodes.registerType("liteputer out", litePuterNode);
};
