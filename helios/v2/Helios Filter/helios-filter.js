const soap = require('soap');

const POLICY_SERVICE_SOAP_PATH = '/PolicyService.svc';
const DEVICE_SERVICE_SOAP_PATH = '/DeviceService.svc';
const GET_POLICY_ACTION = 'http://tempuri.org/IPolicyService/GetPolicy';
const GET_PENDING_FUNCTION_ACTION = 'http://tempuri.org/IDeviceService/GetPendingFunctions';
const FUNCTION_COMPLETED_ACTION = 'http://tempuri.org/IDeviceService/FunctionCompleted';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-filter]';

function getPolicy(url, args, results, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if (client) {
            client.on('request', function (xmlMsg) {
                console.log("getPolicy XML: " + xmlMsg);
            });
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_POLICY_ACTION);
            client.GetPolicy(args.shift(), function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(url, args, results.concat(result));
                }
            });
        }
    }, url);
}

function getPendingFunctions(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_PENDING_FUNCTION_ACTION);

            client.on('request', function (xmlMsg) {
                console.log("Get Pending XML: " + xmlMsg);
            });
            client.GetPendingFunctions(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    if(result) {
                        result.token = args.authenticationToken
                    }
                    resolve(result);
                }
            });
        }
    }, url);
}

function functionCompleted(url, func, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, FUNCTION_COMPLETED_ACTION);
            client.FunctionCompleted(func, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    //resolve(url, functions, result);
                    resolve();
                }
            });
        }
    }, url);
}


module.exports = function (RED) {
    var operators = {
        'eq': function (a, b) {
            return a == b;
        },
        'neq': function (a, b) {
            return a != b;
        },
        'lt': function (a, b) {
            return a < b;
        },
        'lte': function (a, b) {
            return a <= b;
        },
        'gt': function (a, b) {
            return a > b;
        },
        'gte': function (a, b) {
            return a >= b;
        },
        'btwn': function (a, b, c) {
            return a >= b && a <= c;
        },
        'cont': function (a, b) {
            return (a + "").indexOf(b) != -1;
        },
        'regex': function (a, b, c, d) {
            return (a + "").match(new RegExp(b, d ? 'i' : ''));
        },
        'true': function (a) {
            return a === true;
        },
        'false': function (a) {
            return a === false;
        },
        'null': function (a) {
            return (typeof a == "undefined" || a === null);
        },
        'nnull': function (a) {
            return (typeof a != "undefined" && a !== null);
        },
        'else': function (a) {
            return a === true;
        }
    };

    function HeliosFilterNode(config) {
        RED.nodes.createNode(this, config);
        this.policy_rules = config.policy_rules || [];
        this.pending_functions = config.pending_functions || [];

        this.storedPendingFunctions = [];
        this.functionConjuction = 0;
        this.policyConjuction = 0;

        this.checkall = config.checkall || "true";
        var node = this;
        for (var i = 0; i < this.policy_rules.length; i += 1) {
            var rule = this.policy_rules[i];
            if (!rule.vt) {
                if (!isNaN(Number(rule.v))) {
                    rule.vt = 'num';
                } else {
                    rule.vt = 'str';
                }
            }
            if (rule.vt === 'num') {
                if (!isNaN(Number(rule.v))) {
                    rule.v = Number(rule.v);
                }
            }
            if (typeof rule.v2 !== 'undefined') {
                if (!rule.v2t) {
                    if (!isNaN(Number(rule.v2))) {
                        rule.v2t = 'num';
                    } else {
                        rule.v2t = 'str';
                    }
                }
                if (rule.v2t === 'num') {
                    rule.v2 = Number(rule.v2);
                }
            }
        }

        function getFunctions(credentials, completeHandler) {
            getPendingFunctions(credentials.url + DEVICE_SERVICE_SOAP_PATH, {
                authenticationToken: credentials.token,
                deviceFilterList: {
                    DeviceFilterCriteria: {
                        EntityId: credentials.device_id
                    }
                }
            }, function (result) {
                console.log(TAG, 'onGetPendingFunctions result', result);
                if (result &&
                    result.GetPendingFunctionsResult &&
                    result.GetPendingFunctionsResult.Function &&
                    result.GetPendingFunctionsResult.Function.length) {
                    var pendingFunctions = result.GetPendingFunctionsResult.Function;
                    pendingFunctions.map(function(obj){
                        return {token: result.token, data: obj};
                    }).forEach(function (element, index, array) {
                        functionCompleted(credentials.url.url + DEVICE_SERVICE_SOAP_PATH, {
                            authenticationToken: element.token,
                            functionID: element.data.Id
                        }, function() {
                            completeHandler(element.data)
                        }, error);
                    });
                    completeHandler(pendingFunctions);
                }
            }, error);
        }

        this.on('input', function (msg) {
            if (!msg.payload.token || !msg.payload.device_id) {
                node.status({fill: "red", shape: "ring", text: "input is corrupted"});
                return;
            }
            node.input = msg.payload;
            var p = [];
            console.log("policy length: " + node.policy_rules.length);
            for (var i = 0; i < node.policy_rules.length; i++) {
                console.log("policy: " + node.policy_rules[i].n);
                p.push({
                    authenticationToken: msg.payload.token,
                    entityId: msg.payload.device_id,
                    policyName: node.policy_rules[i].n
                });
            }

            if (p.length > 0) {
                policyRequest(msg.payload.url + POLICY_SERVICE_SOAP_PATH, p, []);
            }

            if(node.pending_functions.length > 0) {
                getFunctions({url: msg.payload.url, token: msg.payload.token, device_id: msg.payload.device_id}, function (pendingFunc) {
                    console.log(TAG, 'function result', pendingFunc);
                    checkFunctions(pendingFunc);
                    filter();
                });
            }
        });

        function filter() {
            console.log("onward: ", onward, node.checkall);
            if ((node.checkall == "false" && (node.policyConjuction + node.functionConjuction) > 0) ||
                (node.policyConjuction == node.policy_rules.length && node.functionConjuction == node.pending_functions.length)) {

                node.storedPendingFunctions = [];
                node.functionConjuction = 0;
                node.policyConjuction = 0;

                node.send({payload: node.input});
            }
        }

        function checkPolicies(policies) {
            node.policyConjuction = 0;
            try {
                for (var i = 0; i < policies.length; i++) {
                    var rule = node.policy_rules[i];
                    var prop = evaluateNodeProperty(policies[i].Value, rule.vt);
                    var test = prop;
                    var v1, v2;
                    if (rule.vt === 'prev') {
                        v1 = node.previousValue;
                    } else {
                        v1 = RED.util.evaluateNodeProperty(rule.v, rule.vt, node, null);
                    }
                    v2 = rule.v2;
                    if (rule.v2t === 'prev') {
                        v2 = node.previousValue;
                    } else if (typeof v2 !== 'undefined') {
                        v2 = RED.util.evaluateNodeProperty(rule.v2, rule.v2t, node, null);
                    }
                    node.previousValue = prop;
                    console.log("compare values: ", rule.t, rule.vt, v1, test);
                    if (operators[rule.t](test, v1, v2, rule.case)) {
                        node.policyConjuction++;
                    }
                }
            } catch (err) {
                node.warn(err);
            }
        }

        function checkFunctions(pending) {
            node.functionConjuction = 0;
            node.storedPendingFunctions = node.storedPendingFunctions.concat(pending);
            for (var i = 0; i < node.storedPendingFunctions.length;  i++) {
                if(node.pending_functions.length == i) break;
                if(node.storedPendingFunctions[i].Id == node.pending_functions[i].t) {
                    node.functionConjuction++;
                }
            }
        }

        function policyRequest(url, data, result) {
            if (data && data.length) {
                getPolicy(url, data, result, policyRequest, error)
            } else {
                result = {
                    PolicyList: result.map(function (value) {
                        return value.GetPolicyResult;
                    })
                };
                console.log(TAG, 'policy result', result.PolicyList);

                checkPolicies(result.PolicyList);
                filter();
            }
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    function evaluateNodeProperty(value, type) {
        if (type === 'str') {
            return "" + value;
        } else if (type === 'num') {
            return parseFloat(value);
        } else if (type === 'json') {
            return JSON.parse(value);
        } else if (type === 're') {
            return new RegExp(value);
        } else if (type === 'bool') {
            return /^true$/i.test(value);
        }
        return value;
    }

    RED.nodes.registerType("Filter", HeliosFilterNode);
};
