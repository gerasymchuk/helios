const azure = require('azure-sb');

//const CONNECTION_DEV = "Endpoint=sb://helios-push.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=kxQayHonS0Y1Ckrdju6iXrF4FGLHDHMprna9OK/BHhI=";
//const HUB_NAME = "HeliosNotificationHub";

const TAG = '[helios-mobile-push]';

const NAMES = {
    '10': 'Configuration',
    '15': 'Execute  Script',
    '51': 'Operational Mode Change',
    '52': 'Reset',
    '53': 'Reconciliate',
    '200': 'Message',
    '301': 'Input Change',
    '302': 'Output Change',
    '500': 'Support',
    '900': 'Statistics',
    '60': 'Policy Change',
    '65': 'Mode Change',
    '66': 'Schedule Change',
    '1001': 'Entry',
    '1002': 'Cancelled Entry',
    '1004': 'Allow Entry',
    '1101': 'Temperature Change',
    '1201': 'Cash Deposit',
    '1202': 'Cash Withdrawal',
    '1203': 'Emptying',
    '1204': 'Refill',
    '1205': 'Balance Update',
    '1206': 'Cash Movement',
    '1301': 'Movement',
    '1302': 'Start Recording',
    '1303': 'Stop Recording',
    '1304': 'Activate Surveillance',
    '1305': 'Deactivate Surveillance',
    '1306': 'Retrieve Recording',
    '1307': 'Take Snapshot',
    '1308': 'Alarm',
    '1309': 'Cancel Alarm',
    '1400': 'New Attendee',
    '2501': 'Start Playback',
    '2502': 'Playback Started',
    '2503': 'Screen Overlay',
    '3001': 'Weather Change'
};

module.exports = function (RED) {
    function HeliosMobilePush(config) {
        RED.nodes.createNode(this, config);

        const node = this;

        this.on('input', function(msg) {
            node.status({});
            var data = {};
            if (config.data_source == 'payload') {
                // Sample:
                // {
                //     "Endpoint": "",
                //     "ApiKey": "",
                //     "HubName": "",
                //     "Sound": "",
                //     "Message": "",
                //     "Parameters": {
                //     "EntityId": "",
                //         "EntityType": "",
                //         "Alarm": {
                //         "ErrorCode": "",
                //             "StatusID": "",
                //             "AlarmStatus": "",
                //             "Message": "",
                //             "PriorityId": ""
                //     }
                // }
                if (msg.payload) {
                    data = msg.payload;
                } else {
                    return;
                }
            } else {
                data = {
                    UserApiKey: config.user_api_key,
                    Endpoint: config.url,
                    ApiKey: config.api_key,
                    HubName: config.hub_name,
                    Message: config.message,
                    Sound: config.sound,
                    Parameters: {
                        EntityId: config.entity_id,
                        EntityType: config.entity_type
                    }};

                if (config.data_source == 'properties') {
                    if (config.entity == 'function' || config.entity == 'event') {
                        const params = config.params.map(function(obj){
                            return {Details: '', Name: obj.Name, Type: {Id: obj.Id}, Value: obj.Value};
                        });
                        const entity = {
                            Name: NAMES[config.action_type.toString()],
                            ParameterList: params,
                            TypeId: config.action_type
                        };
                        if (config.entity == 'function') {
                            data.Parameters.Function = entity;
                        } else {
                            data.Parameters.Event = entity;
                        }
                    } else if (config.entity == 'alarm') {
                        data.Parameters.Alarm = {
                            ErrorCode: config.error_code,
                        };
                        if(config.status_id) {
                            data.Parameters.Alarm.StatusID = config.status_id;
                            data.Parameters.Alarm.AlarmStatus = config.status_id;
                        }
                        if(config.alarm_message) {
                            data.Parameters.Alarm.Message = config.alarm_message;
                        }
                        if(config.priority_id) {
                            data.Parameters.Alarm.PriorityId = config.priority_id;
                        }
                    }
                } else {
                    if (config.entity == 'function') {
                        data.Parameters.Function = msg.payload;
                    } else  if (config.entity == 'event') {
                        data.Parameters.Event = msg.payload;
                    } else if (config.entity == 'alarm') {
                        data.Parameters.Alarm = msg.payload;
                    }
                }
            }
            const payload = {
                aps: {alert: data.Message, sound: data.Sound},
                param: data.Parameters
            };
            console.log("Payload: " + JSON.stringify(payload));
            const connectionString = "Endpoint=" + data.Endpoint + ";SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=" + data.ApiKey;
            const notificationHubService = azure.createNotificationHubService(data.HubName, connectionString);
            const tag = data.UserApiKey || null;
            console.log("URL: " + connectionString + "\nhub name:"+ data.HubName + "\ntag: " + tag);
            notificationHubService.apns.send(tag, payload, function(error){
                if(error){
                    node.status({fill: "red", shape: "ring", text: error && JSON.stringify(error)});
                    node.error(error);
                } else {
                    node.status({fill:"green",shape:"dot",text:"send"});
                    console.log("Notification is sent!");
                }
            });
        });
    }

    RED.nodes.registerType("Mobile Push", HeliosMobilePush);
};
