const artnet = require('artnet-node');

module.exports = function(RED) {

    const IS_DEBUG = false;

    const MIN_CHANNEL_VALUE = 0;
    const MAX_CHANNEL_VALUE = 255;

    const OZ = {x:0, y:0, z:-1};

    var nodeData = undefined;
    var calculatedNodeData = undefined;

    const DEGREE_TO_CHANNEL_COEFFICIENT = 255/540;
    const DEGREE_TO_CHANNEL_COEFFICIENT2 = 255/540;
    const CHANNEL_TO_DEGREE_COEFFICIENT = 540/255;
    const CHANNEL_TO_DEGREE_COEFFICIENT2 = 540/255;

    const DEGREE_TO_RAD = Math.PI/180;
    const RAD_TO_DEGREE = 180/Math.PI;

    const PAN_INDEX = 1;
    const TILT_INDEX = 3;

    // region utils
    function tracePoint(tag,point){
        log(tag + " : " + parseFloat(point.x).toFixed(4) + " : " + parseFloat(point.y).toFixed(4) + " : " + parseFloat(point.z).toFixed(4));
    }

    function roundChannelValue(value){
        return Math.min(MAX_CHANNEL_VALUE,Math.max(MIN_CHANNEL_VALUE,Math.round(value)));
    }

    function validateChannelValue(value){
        return Math.min(MAX_CHANNEL_VALUE,Math.max(MIN_CHANNEL_VALUE,value));
    }

    var epsilon = Math.exp(-16);
    Math.Cos = function(w){
        return parseFloat(Math.cos(w).toFixed(10));
    };

    Math.Sin = function(w){
        return parseFloat(Math.sin(w).toFixed(10));
    };

    function degreesToRadian(v) {
        return ((v + 360)% 360) * DEGREE_TO_RAD;
    }

    function radiansToDegrees(v) {
        return v * RAD_TO_DEGREE;
    }

    function channelValueToRad(v,isTilt){
        var angle = v * (isTilt ? CHANNEL_TO_DEGREE_COEFFICIENT2 : CHANNEL_TO_DEGREE_COEFFICIENT);
        return degreesToRadian(angle);
    }

    function radToChannelValue(v,isTilt) {
        return radiansToDegrees(v) * (isTilt ?  DEGREE_TO_CHANNEL_COEFFICIENT2 : DEGREE_TO_CHANNEL_COEFFICIENT);
    }
    //endregion

    //region vector utils
    function getVector(p1, p2) {
        return {x:(p2.x - p1.x), y:(p2.y - p1.y), z:(p2.z - p1.z)};
    }

    function vectorModule(v){
        return Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    }

    function vectorScalar(v1, v2){
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    }

    function normalizeVector(v){
        var _vectorModule = vectorModule(v);
        return {x:v.x / _vectorModule, y:v.y / _vectorModule, z:v.z / _vectorModule};
    }

    function vectorsAngle(v1, v2){
        return Math.acos(vectorScalar(v1,v2) / (vectorModule(v1) * vectorModule(v2)));
    }

    function getNormalVector(p1, p2, p3){
        var p1p2Vect = getVector(p1,p2);
        var p1p3Vect = getVector(p1,p3);
        return getVectorMultiplication(p1p2Vect,p1p3Vect);
    }

    function getVectorMultiplication(p1p2Vect, p1p3Vect){
        var i_v = p1p2Vect.y*p1p3Vect.z - p1p2Vect.z*p1p3Vect.y;
        var j_v = - (p1p2Vect.x*p1p3Vect.z - p1p2Vect.z*p1p3Vect.x);
        var k_v = p1p2Vect.x*p1p3Vect.y - p1p2Vect.y*p1p3Vect.x;
        return {x:i_v, y:j_v, z:k_v};
    }
    //endregion

    // region spherical to cartesian coordinates
    // phi is in [0,2*pi]
    // theta is in [0,pi]
    function toCartesian(point) {
        var _point = {};
        var r = 10;
        _point.x = r * Math.Sin(point.theta) * Math.Cos(point.phi);
        _point.y = r * Math.Sin(point.theta) * Math.Sin(point.phi);
        _point.z = r * Math.Cos(point.theta);
        return _point;
    }

    // convert cartesian to spherical coordinates
    function toSpherical(point) {
        var _point = {};
        _point.r = Math.sqrt(point.x*point.x + point.y*point.y + point.z*point.z);
        if(point.x == 0 && point.y == 0){
            _point.theta = point.z >= 0 ? 0 : Math.PI;
            _point.phi = 0;
        } else {
            _point.theta = Math.acos(point.z / _point.r);
            _point.phi = (Math.atan2(point.y, point.x) + 2*Math.PI)%(2*Math.PI);
        }
        return _point;
    }

    function movePointInSpherical(point, theta){
        point.theta -= theta;
        if(point.theta < 0){
            point.theta = -point.theta;
            point.phi += Math.PI;
        }
        return point;
    }
    function movePointInCartesian(p1,p2, coef){
        return {x:p1.x + coef * p2.x,y:p1.y + coef * p2.y,z:p1.z + coef * p2.z};
    }

    function getDistanceBetweenPointsInSpherical(p1, p2){
        var R = 1;
        var delta_phi = p2.phi - p1.phi;
        var delta_theta = p2.theta - p1.theta;
        var a = Math.Sin(delta_phi/2) * Math.Sin(delta_phi/2)
            + Math.Cos(p1.phi) * Math.Cos(p2.phi)
            * Math.Sin(delta_theta/2) * Math.Sin(delta_theta/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;

        return d;
    }
    function getDistanceBetweenPointsInCartesian(point1, point2){
        var xDelta = point1.x - point2.x;
        var yDelta = point1.y - point2.y;
        var zDelta = point1.z - point2.z;
        return Math.sqrt(xDelta*xDelta + yDelta*yDelta + zDelta*zDelta);
    }
    //endregion

    //region circle main formulas

    // Find center point C0 what is on vector OC and on ABC plane
    // A,B - points on the sphere
    // C - projection of the center point on the sphere
    function calcCenterPoint(centerOnSphere, p) {
        var moduleSqr = vectorScalar(centerOnSphere, centerOnSphere);
        var coef = 1 - (moduleSqr-vectorScalar(centerOnSphere, p))/moduleSqr;
        return {
            x: centerOnSphere.x * coef,
            y: centerOnSphere.y * coef,
            z: centerOnSphere.z * coef};
    }


    // spherical coordinates => polar coordinates
    // circle plane is parallel with Oxy plane => z=0; theta = pi/2
    // sin(theta) = sin(pi/2) = 1;
    // cos(theta) = cos(pi/2) = 0;
    // x = r*sin(theta)*cos(phi) = r*cos(phi)
    // y = r*sin(theta)*sin(phi) = r*sin(phi)
    // z = r*cos(theta) = 0;
    function getCirclePoint2(t, radius){
        return {x:radius * Math.cos(t), y:radius*Math.sin(t), z:0};
    }

    /*
     * Rotate coordinate system by angle
     * enjoy : http://math.stackexchange.com/questions/542801/rotate-3d-coordinate-system-such-that-z-axis-is-parallel-to-a-given-vector
     */
    function rotatePoint_xy_quarterion(p, normalVector){
        var angle = vectorsAngle(OZ,normalVector);
        // find case when angle near 0 or PI
        var an = (angle + 2*Math.PI)%(Math.PI);
        if(an < epsilon || an > Math.PI - epsilon){
            return p;
        }

        var b = getVectorMultiplication(OZ,normalVector);
        b = normalizeVector(b);

        var q0 = Math.cos(angle/2);
        var q1 = Math.sin(angle/2)*b.x;
        var q2 = Math.sin(angle/2)*b.y;
        var q3 = Math.sin(angle/2)*b.z;

        var rm = [
            [q0*q0 + q1*q1 - q2*q2 - q3 *q3 ,   2*(q1*q2 - q0*q3),                  2*(q1*q3 + q0*q2)],
            [2*(q1*q2 + q0*q3),                 q0*q0 - q1*q1 + q2*q2 - q3 *q3 ,    2*(q2*q3 - q0*q1)],
            [2*(q1*q3 - q0*q2),                 2*(q2*q3 + q0*q1),                  q0*q0 - q1*q1 - q2*q2 + q3 *q3]];

        var res = {};
        res.x = p.x* rm[0][0] + p.y * rm[1][0] + p.z * rm[2][0];
        res.y = p.x* rm[0][1] + p.y * rm[1][1] + p.z * rm[2][1];
        res.z = p.x* rm[0][2] + p.y * rm[1][2] + p.z * rm[2][2];

        return res;
    }
    //endregion

    function log() {
        if(IS_DEBUG) {
            console.log(arguments.join(" "));
        }
    }

    function ArtnetOutNode(config) {
        RED.nodes.createNode(this, config);
        this.flowContext = this.context().global;

        this.name = config.name;
        this.address = config.address;
        this.port = config.port || 6454;
        this.rate = config.rate || 40;
        this.size = config.size || 512;
        this.universe = config.universe || 0;

        this.client = artnet.Client.createClient(this.address, this.port);
        this.timeoutsMap = [];
        this.closeCallbacksMap = {};
        var node = this;

        this.data = function (){
            return this.flowContext.get("nodeData") || [];
        };

        this.calculatedData = function(){
            return this.flowContext.get("calculatedNodeData") || [];
        };

        this.clearTimeouts = function (){
            for (var i = 0; i < node.timeoutsMap.length; i++) {
                clearTimeout(node.timeoutsMap[i]);
            }
            node.timeoutsMap.length = 0;
        };

        this.sendData = function(){
            node.client.send(this.data());
        };

        this.set = function(address, value, transition, transition_time) {
            if(address > 0){
                if(transition){
                    node.fadeToValue(address, parseInt(value), transition_time);
                } else {
                    var nodeData = this.data();
                    nodeData[address-1] = roundChannelValue(value);
                    var calculatedNodeData = this.calculatedData();
                    calculatedNodeData[address-1] = validateChannelValue(value);

                    this.flowContext.set("nodeData",nodeData);
                    this.flowContext.set("calculatedNodeData",calculatedNodeData);
                    //node.data[address-1] = roundChannelValue(value);
                    //node.calculatedData[address-1] = validateChannelValue(value);
                }
            }
        };

        node.get = function(address){
            var nodeData = this.flowContext.get("nodeData") || [];
            return parseInt(nodeData[address-1] || 0);
        };

        node.getCalculated = function(address){
            var calculatedData = this.flowContext.get("calculatedNodeData") || [];
            var nodeData = this.flowContext.get("nodeData") || [];
            var calculated  = calculatedData[address-1] || 0;
            return calculated  || parseInt(nodeData[address-1] || 0);
        };

        this.on('input', function(msg) {
            closeFunction(false);
            var payload = msg.payload;

            var transition = payload.transition;
            var duration = parseInt(payload.duration || 0);

            this.universe = payload.universe || config.universe || 0;
            this.client.UNVERSE = [this.universe, 0];

            if(payload.start_buckets && Array.isArray(payload.start_buckets)){
                for(var i = 0; i < payload.start_buckets.length; i++) {
                    node.set(payload.start_buckets[i].channel, payload.start_buckets[i].value);
                }
                node.sendData();
            }

            if(transition == "arc"){
                try{
                    if(!payload.end || !payload.center) {
                        node.error("Invalid payload");
                    }

                    node.channels = payload.channels || {pan: PAN_INDEX, tilt: TILT_INDEX};

                    var cv_phi = payload.hasOwnProperty("start") ? payload.start.pan : channelValueToRad(node.getCalculated(PAN_INDEX),false); // pan
                    var cv_theta = payload.hasOwnProperty("start") ? payload.start.tilt : channelValueToRad(node.getCalculated(TILT_INDEX),true); //tilt

                    var interval = {start: 0, end: 1};
                    if(Array.isArray(payload.interval) && payload.interval.length > 1) {
                        interval.start = payload.interval[0];
                        interval.end = payload.interval[1];
                    }

                    node.moveToPointOnArc(
                        cv_theta,cv_phi,
                        payload.end.tilt,payload.end.pan,
                        payload.center.tilt, payload.center.pan,
                        duration,interval);
                } catch (e){
                    log("ERROR " + e.message);
                }
            } else{
                if(payload.channel) {
                    node.set(payload.channel, payload.value, transition, duration);
                } else if(Array.isArray(payload.buckets)) {
                    for(var i = 0; i < payload.buckets.length; i++) {
                        node.set(payload.buckets[i].channel, payload.buckets[i].value,transition,duration);
                    }
                    if(!transition) {
                        node.sendData()
                    }
                }
            }
        });

        function closeFunction(allowResend){
            node.clearTimeouts();

            var resend = false;
            for (var name in node.closeCallbacksMap){
                if(node.closeCallbacksMap.hasOwnProperty(name)){
                    node.closeCallbacksMap[name]();
                    resend = true;
                }
            }

            if(resend && allowResend){
                node.sendData();
            }

            node.closeCallbacksMap = {};
        };

        this.on("close",function () {
            closeFunction(true);
        });

        this.fadeToValue = function(channel, new_value, transition_time) {
            var oldValue = node.get(channel);
            var steps = transition_time / node.rate;

            // calculate difference between new and old values
            var diff = Math.abs(oldValue -  new_value);
            if(diff/steps <= 1){
                steps = diff;
            }

            // should we fade up or down?
            var direction = (new_value > oldValue);

            var value_per_step = diff / steps;
            var time_per_step = transition_time / steps;

            var timeoutID;
            for (var i = 1; i < steps; i++) {
                // create time outs for each step
                timeoutID = setTimeout(function () {
                    var valueStep = direction === true ? value_per_step : -value_per_step;
                    var value = Math.round(node.get(channel) + valueStep);
                    node.set(channel, value);
                    node.sendData();
                }, i * time_per_step);
                node.timeoutsMap.push(timeoutID);
            }

            timeoutID = setTimeout(function() {
                node.set(channel, new_value);
                node.sendData();
                delete node.closeCallbacksMap[channel];
            }, transition_time);
            node.timeoutsMap.push(timeoutID);

            // add close callback to set channels to new_value in case redeploy and all timeouts stopping
            node.closeCallbacksMap[channel] = (function () {
                node.set(channel, new_value);
                node.sendData();
            });
        };

        this.moveToPointOnArc = function(_cv_theta, _cv_phi, _tilt_nv, _pan_nv, _tilt_center, _pan_center, transition_time, interval){
            // current value
            var cv_theta = channelValueToRad(_cv_theta,true); //tilt
            var cv_phi = channelValueToRad(_cv_phi,false); // pan

            // target value
            var nv_theta = channelValueToRad(_tilt_nv,true);
            var nv_phi = channelValueToRad(_pan_nv,false);

            // center value
            var tilt_center = channelValueToRad(_tilt_center,true); //tilt
            var pan_center = channelValueToRad(_pan_center,false); // pan

            log("Input points ", "\n curPoint:" ,cv_theta,cv_phi,"\n " +
                "newPoint: ",nv_theta,nv_phi, "\n" +
                "newPoint2: ",radiansToDegrees(nv_theta),radiansToDegrees(nv_phi), "\n" +
                "centerPoint: ",tilt_center,pan_center);
            log("*************************************");
            // convert points to Cartesian coordinate system
            log("1 -> convert  points to cartesian \n");
            var currentPoint = toCartesian({phi:cv_phi, theta:cv_theta});
            var newPoint = toCartesian({phi:nv_phi, theta:nv_theta});
            var centerPoint = toCartesian({phi:pan_center, theta:tilt_center});
            var vn = centerPoint;
            vn = normalizeVector(vn);
            centerPoint = calcCenterPoint(centerPoint, currentPoint);

            tracePoint("currentPoint ",currentPoint);
            tracePoint("newPoint     ",newPoint);
            tracePoint("centerPoint  ",centerPoint);
            log("*************************************");
            var movement_point = centerPoint;

            // move center of circle to center of coordinates
            log("2 -> move to O(0,0,0) \n");
            currentPoint = movePointInCartesian(currentPoint,centerPoint,-1);
            newPoint = movePointInCartesian(newPoint,centerPoint,-1);
            centerPoint = movePointInCartesian(centerPoint,centerPoint,-1);
            tracePoint("currentPoint ",currentPoint);
            tracePoint("newPoint     ",newPoint);
            tracePoint("centerPoint  ",centerPoint);
            log("*************************************");

            // calculate normal vector (i,j,k) for circle plane (three points)
            log("3 -> normal vector calculation \n");
            //var vn = getNormalVector(centerPoint,currentPoint,newPoint);
            //vn = normalizeVector(vn);
            tracePoint("normalVector ",vn);
            var backVector = rotatePoint_xy_quarterion(OZ,vn);
            tracePoint("BackVector",backVector);
            log("*************************************");

            log("4 -> rotate coordinate system \n");
            currentPoint = rotatePoint_xy_quarterion(currentPoint,vn);
            newPoint = rotatePoint_xy_quarterion(newPoint ,vn);
            centerPoint = rotatePoint_xy_quarterion(centerPoint,vn);

            tracePoint("currentPoint ",currentPoint);
            tracePoint("newPoint     ",newPoint);
            tracePoint("centerPoint  ",centerPoint);
            log("*************************************");


            log("4.1 -> rotate coordinate system back for check\n");
            var currentPoint1 = rotatePoint_xy_quarterion(currentPoint,backVector);
            var newPoint1 = rotatePoint_xy_quarterion(newPoint ,backVector);
            var centerPoint1 = rotatePoint_xy_quarterion(centerPoint,backVector);

            tracePoint("currentPoint1 ",currentPoint1);
            tracePoint("newPoint1     ",newPoint1);
            tracePoint("centerPoint1  ",centerPoint1);
            log("*************************************");

            var radius = getDistanceBetweenPointsInCartesian(currentPoint,centerPoint);
            var radius2 = getDistanceBetweenPointsInCartesian(newPoint,centerPoint);
            if(Math.abs(radius2 - radius) > epsilon){
                node.error("Invalid center point");
                return;
            }
            //TODO check is points collinear

            log("5 -> parametric equation startT and endT calculation \n");
            //find t parameter for start and end point
            var currentT = (Math.acos(currentPoint.x/radius)+2*Math.PI)%(2*Math.PI);
            var newT = (Math.acos(newPoint.x/radius)+2*Math.PI)%(2*Math.PI);
            log("T parameters",currentT, newT);

            //for 3 and 4 quarter
            /*if(currentPoint.y < 0){
             currentT = 2*Math.PI-currentT;
             }*/

            //for 3 and 4 quarter
            /*if(newPoint.y < 0){
             newT = 2*Math.PI-newT;
             }*/
            log("T parameters rad", currentT, newT);
            log("T parameters degree", radiansToDegrees(currentT), radiansToDegrees(newT));
            log("*************************************");

            var actualAngle = newT - currentT;
            var angleDelta = Math.abs(actualAngle) <= Math.PI ? actualAngle :  -(actualAngle - Math.PI);

            var steps = transition_time / node.rate;
            var time_per_step = node.rate;
            var angleStep = angleDelta/steps;
            // limit steps for interval
            var startStep = parseInt(steps * interval.start);
            var endStep = parseInt(steps * interval.end);
            log("angleStep", angleDelta, angleStep,radiansToDegrees(angleDelta));
            log("angleStep",steps,startStep,endStep);

            var timeoutID;
            var counter = 0;
            for (var i = startStep; i <= endStep; i++) {
                var t = currentT + i*angleStep;
                timeoutID = setTimeout(function (t) {
                    var iterationPoint = getCirclePoint2(t,radius);
                    log("iterationPoint1 ","x: ",parseFloat(iterationPoint.x).toFixed(4), "y:    ",parseFloat(iterationPoint.y).toFixed(4), "z:  ",parseFloat(iterationPoint.z).toFixed(4));
                    iterationPoint = rotatePoint_xy_quarterion(iterationPoint,backVector);
                    log("iterationPoint2 ","x: ",parseFloat(iterationPoint.x).toFixed(4), "y:    ",parseFloat(iterationPoint.y).toFixed(4), "z:  ",parseFloat(iterationPoint.z).toFixed(4));
                    iterationPoint = movePointInCartesian(iterationPoint, movement_point,1);

                    var p  = toSpherical({x:iterationPoint.x,y:iterationPoint.y, z:iterationPoint.z});
                    var tilt = validateChannelValue(radToChannelValue(p.theta, true));
                    var pan = validateChannelValue(radToChannelValue(p.phi, false));

                    log("iterationPoint3 ","x: ",parseFloat(iterationPoint.x).toFixed(4), "y:    ",parseFloat(iterationPoint.y).toFixed(4), "z:  ",parseFloat(iterationPoint.z).toFixed(4),"\n",
                        "sphericalP     ","r: ",parseFloat(p.r).toFixed(4),              "theta:",parseFloat(p.theta).toFixed(4),          "phi:",parseFloat(p.phi).toFixed(4),"\n",
                        "T:             ", parseFloat(t).toFixed(4),"\n",
                        "TILT: ",tilt,"pan: ",pan);
                    log("**********************");
                    node.set(TILT_INDEX, tilt);
                    node.set(PAN_INDEX, pan);
                    node.sendData();
                }, counter*time_per_step , t);
                counter++;
                node.timeoutsMap.push(timeoutID);
            }

            if(endStep == 1){
                var tilt = validateChannelValue(radToChannelValue(nv_theta, true));
                var pan = validateChannelValue(radToChannelValue(nv_phi, false));
                timeoutID = setTimeout(function() {
                    node.set(TILT_INDEX, tilt);
                    node.set(PAN_INDEX, pan);
                    node.sendData();
                    delete node.closeCallbacksMap[TILT_INDEX + "_" + PAN_INDEX];
                }, transition_time);
                node.timeoutsMap.push(timeoutID);

                // add close callback to set channels to new_value in case redeploy and all timeouts stopping
                node.closeCallbacksMap[TILT_INDEX + "_" + PAN_INDEX] = (function () {
                    node.set(TILT_INDEX, tilt);
                    node.set(PAN_INDEX, pan);
                    node.sendData();
                });
            }
        }
    }

    RED.nodes.registerType("artnet out", ArtnetOutNode);
};