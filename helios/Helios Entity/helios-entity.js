//region utils
const soap = require('soap');
const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

const WSDL = require('soap/lib/wsdl').WSDL;
const orig = WSDL.prototype.objectToXML;
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]);
    if (xml.indexOf("FindDevice") > -1) {
        xml = orderAlphabetically(xml, 'alarm');
    }
    return xml;
};

function orderAlphabetically(xml, nodeName) {
    var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
    var p = d.getElementsByTagName(nodeName);
    Array.prototype.slice.call(p).forEach(function (element, index, array) {
        Array.prototype.slice.call(element.childNodes)
            .sort(function (x, y) {
                if (x.nodeName < y.nodeName) return -1;
                if (x.nodeName > y.nodeName) return 1;
                return 0;
            }).forEach(function (x) {
            element.appendChild(x);
        });
    });
    return xmlserializer.serializeToString(d);
}

function filterArrayByProp(array, propName){
    for (var i = 0; i < array.length; i++) {
        var obj = array[i];
        if(obj["key"] === propName){
            return obj["value"];
        }
    }
    return "";
}

function traceDevices(devices) {
    if(Array.isArray(devices)){
        devices.forEach(function(element) {
            var props = element.Properties.KeyValuePairOfstringstring;
            console.log("Entity", element.EntityId, element.ParentEntityId, filterArrayByProp(props,"ApiKey"));
        });
    }
}

const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';


const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const ENTITY_SERVICE_SOAP_PATH = '/DeviceService.svc';

const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';

const FIND_DEVICE_ACTION = 'http://tempuri.org/IDeviceService/FindDevice';
const GET_RELATED_ENTITIES_ACTION = 'http://tempuri.org/IDeviceService/GetRelatedEntities';

const TAG = '[helios-entity]';


function authorize(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if (client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
            client.Authorize(args, function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                    console.log("entityID", result.AuthorizeResult.EntityId);
                    resolve({
                        token: result.AuthorizeResult.AuthenticationToken,
                        device_id: result.AuthorizeResult.EntityId
                    });
                }
            });
        }
    }, url);
}

//endregion

var entId;

var FUNCTIONS_OBJECTS = [
    {func:"FindDevice", action:FIND_DEVICE_ACTION},
    {func:"FindDevice", action:FIND_DEVICE_ACTION},
    {func:"FindDevice", action:FIND_DEVICE_ACTION},
    {func:"GetRelatedEntities", action:GET_RELATED_ENTITIES_ACTION}
];

function callFunction(url, args, funcObj, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if (client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, funcObj.action);
            client[funcObj.func](args, function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }
    }, url);
}

var ARGUMENTS_FUNCTIONS = [
    getFindDeviceArgs,
    getAllChildDevicesArgs,
    getFindDeviceArgs,
    getRelatedEntitiesArgs
];
function getFindDeviceArgs(authenticationToken, entityId) {
    var argsRes = {authenticationToken: authenticationToken};
    if (entityId) {
        argsRes.device = {EntityId: entityId};
    }
    return argsRes;
}
function getAllChildDevicesArgs(authenticationToken, parentEntityId) {
    var argsRes = {authenticationToken: authenticationToken};
    if (parentEntityId) {
        argsRes.device = {ParentEntityId: parentEntityId};
    }
    return argsRes;
}
function getRelatedEntitiesArgs(authenticationToken, entityId) {
    var argsRes = {authenticationToken: authenticationToken};
    if (entityId) {
        argsRes.entityId = entityId;
    }
    return argsRes;
}

var RESULT_PARSING_FUNCTIONS = [
    parseResultForFindDevice,
    parseResultForFindDevice,
    parseResultForFindDevice,
    parseResultForRelatedEntities
];

function parseResultForFindDevice(result) {
    if (result && result.FindDeviceResult && result.FindDeviceResult.Device) {
        traceDevices(result.FindDeviceResult.Device);
        return result.FindDeviceResult.Device;
    }
    return null;
}
function parseResultForRelatedEntities(result) {
    if (result && result.GetRelatedEntitiesResult && result.GetRelatedEntitiesResult.RelatedEntity) {
        var resObj = result.GetRelatedEntitiesResult.RelatedEntity;
        if(Array.isArray(resObj)){
            resObj.forEach(function(element) {
                console.log("RelatedEntity", element.ApiKey, element.EntityId, element.RelatedEntityId);
            });
        }
        return resObj;
    }
    return null;
}

module.exports = function (RED) {
    function HeliosEntity(config) {
        RED.nodes.createNode(this, config);

        const node = this;

        node.data_source = config.data_source;
        node.method_id = config.method_id;
        node.entity_config  = config.entity_config;
        node.entity_id = config.entity_id;

        const authConfig = RED.nodes.getNode(config.auth);

        this.on('input', function (msg) {
            if (!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});
            if (node.data_source == 'payload') {
                console.log("Input: ", msg);
                if (msg.payload) {
                    node.entity = msg.payload;
                } else {
                    return;
                }
            } else {
                node.entity = {Action: config.method_id};
                if (config.entity_id) {
                    node.entity.EntityId = config.entity_id;
                }
            }

            console.log("start authorize ", authConfig.credential_type, authConfig.username, node.entity.EntityId);
            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error);
        });



        function onAuthorized(credentials) {
            node.status({fill: "green", shape: "dot", text: "connected"});
            node.apiKey = credentials.token;

            var actionObject = FUNCTIONS_OBJECTS[node.entity.Action - 1];
            var argumentsFunction = ARGUMENTS_FUNCTIONS[node.entity.Action - 1];

            if(node.data_source == 'properties'){
                node.entity.EntityId = node.entity_config == "auth" ? credentials.device_id : node.entity.EntityId;
            }

            entId = node.entity.EntityId;

            var args = argumentsFunction(credentials.token, node.entity.EntityId);
            callFunction(config.url + ENTITY_SERVICE_SOAP_PATH,args,actionObject,
                node.entity.Action == 3 ? findDeviceCallback : serverCallback, error);
        }

        function findDeviceCallback(result) {
            console.log(TAG, result);
            var actionObject = FUNCTIONS_OBJECTS[node.entity.Action - 1];
            var resultFunction = RESULT_PARSING_FUNCTIONS[node.entity.Action - 1];
            var res = resultFunction(result);
            if (res && res.length > 0) {
                var args = getFindDeviceArgs(node.apiKey, res[0].ParentEntityId);
                callFunction(config.url + ENTITY_SERVICE_SOAP_PATH,args,actionObject,serverCallback, error);
            } else {
                node.send(null);
            }
        }

        function serverCallback(result){
            console.log(TAG, result);
            var resultFunction = RESULT_PARSING_FUNCTIONS[node.entity.Action - 1];
            var res = resultFunction(result);
            if (res) {
                node.send({payload: res});
            }
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Entity", HeliosEntity);
};
