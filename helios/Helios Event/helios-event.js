const soap = require('soap');
const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

const WSDL = require('soap/lib/wsdl').WSDL;
const orig = WSDL.prototype.objectToXML;
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]).replace('xsi:nil="true"', '');
    if (xml.indexOf("SendEvent") > -1) {
        xml = orderAlphabetically(xml, "q1:Event");
        xml = orderAlphabetically(xml, "q1:Parameter");
    }
    return xml;
};

function orderAlphabetically(xml, nodeName) {
    var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
    var p = d.getElementsByTagName(nodeName);
    Array.prototype.slice.call(p).forEach(function (element, index, array) {
        Array.prototype.slice.call(element.childNodes)
            .sort(function (x, y) {
                if (
                    x.nodeName < y.nodeName) return -1;
                if (x.nodeName > y.nodeName) return 1;
                return 0;
            })
            .forEach(function (x) {
                element.appendChild(x);
            });
    });
    return xmlserializer.serializeToString(d);
}

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const DEVICE_SERVICE_SOAP_PATH = '/DeviceService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const SEND_EVENT_ACTION = 'http://tempuri.org/IDeviceService/SendEvent';
const GET_PENDING_EVENT_ACTION = 'http://tempuri.org/IDeviceService/GetPendingEvents';
const CLEAR_EVENT_ACTION = 'http://tempuri.org/IDeviceService/ClearPendingEvent';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-event]';

function authorize(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if (client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
            client.Authorize(args, function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                    resolve({
                        token: result.AuthorizeResult.AuthenticationToken,
                        device_id: result.AuthorizeResult.EntityId
                    });
                }
            });
        }
    }, url);
}

function sendEvent(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if (client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SEND_EVENT_ACTION);

            client.on('request', function (xmlMsg) {
                console.log("Send Event XML: " + xmlMsg);
            });

            client.SendEvent(args, function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    //resolve(url, args, result);
                    resolve();
                }
            });
        }
    }, url);
}

function getPendingEvents(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_PENDING_EVENT_ACTION);

            client.on('request', function (xmlMsg) {
                console.log("Get Pending XML: " + xmlMsg);
            });
            client.GetPendingEvents(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    if(result) {
                        result.token = args.authenticationToken
                    }
                    resolve(result);
                }
            });
        }
    }, url);
}


function eventClear(url, func, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client) {
            // client.on('request', function (xmlMsg) {
            //     console.log("Event Clear XML: " + xmlMsg);
            // });
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, CLEAR_EVENT_ACTION);
            client.ClearPendingEvent(func, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve();
                }
            });
        }
    }, url);
}

module.exports = function (RED) {

    function HeliosEventInNode(config) {
        RED.nodes.createNode(this, config);
        this.url = config.url;
        this.repeat_interval = config.repeat_interval;
        this.count = config.count;
        var node = this;
        this.interval_id = null;

        const authConfig = RED.nodes.getNode(config.auth);

        this.interval_id = setInterval(function(){
            node.emit("input", {});
        }, this.repeat_interval * 1000);

        this.on('input', function(msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});
            authorize(this.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error)
        });

        function onAuthorized(credentials) {
            node.status({fill:"green",shape:"dot",text:"connected"});
            getPendingEvents(node.url + DEVICE_SERVICE_SOAP_PATH, {
                authenticationToken: credentials.token,
                entityId: credentials.device_id,
                count: node.count
            }, onGetPendingEvent, error);
        }

        function onGetPendingEvent(result){
            console.log(TAG, 'onGetPendingEvent result', result);
            if (result && result.GetPendingEventsResult && result.GetPendingEventsResult.Event && result.GetPendingEventsResult.Event.length) {
                result.GetPendingEventsResult.Event.map(function(obj){
                    return {token: result.token, data: obj};
                }).forEach(processPendingEvent);
            }
        }

        function processPendingEvent(element, index, array) {
            console.log(JSON.stringify(element));
            eventClear(node.url + DEVICE_SERVICE_SOAP_PATH, {
                authenticationToken: element.token,
                eventId: element.data.Id
            }, function() {
                node.send({payload: element.data})
            }, error);
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Event in", HeliosEventInNode);

    HeliosEventInNode.prototype.close = function() {
        if (this.interval_id != null) {
            clearInterval(this.interval_id);
        }
    };


    function HeliosEventOutNode(config) {

        RED.nodes.createNode(this, config);

        this.authConfig = RED.nodes.getNode(config.auth);

        this.url = config.url;

        this.data_source = config.data_source;
        this.event = config.event;
        this.event_type = config.event_type;
        this.event_params = config.event_params;
        var node = this;

        this.on('input', function (msg) {
            if (!node.authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});
            if (node.data_source == 'payload') {
                console.log("Event input: ", msg);
                // Sample:
                //({ "Function": [
                // { "Id": 6922, "Name": "Output Change", "ParameterList":
                //                                          { "Parameter": [
                //                                              { "Name": "Power State", "Type": { "Id": 2, "Name": "String", "Required": false } },
                //                                              { "Name": "Level", "Type": { "Id": 2, "Name": "String", "Required": false }, "Value": "0.6" }
                //                                          ] },
                // "ReferenceId": 0, "TypeId": 302 },
                // ....]}).
                if (msg.payload) { // && msg.payload.Function
                    node.event = msg.payload;
                    // if(!node.event.ParameterList) {
                    //     delete node.event.ParameterList;
                    // }
                } else {
                    return;
                }
            } else {
                // = {Id: node.event_type, ParameterList: [{Parameter}]}
                node.event = {TypeId: node.event_type};
                if (node.event_params.length) {
                    node.event.ParameterList = {Parameter: []};

                    node.event_params.forEach(function (element, index, array) {
                        node.event.ParameterList.Parameter.push({
                            Name: element.Name,
                            Type: {Id: element.Id},
                            Value: element.Value
                        });
                    });
                } else {
                    node.event.ParameterList = null;
                }

            }

            console.log("url: " + node.url);
            console.log("auth: " + node.authConfig);
            console.log("username: " + node.authConfig.username);
            console.log("password: " + node.authConfig.password);
            console.log("credential_type: " + node.authConfig.credential_type);

            console.log("source: " + node.data_source);
            console.log("event: " + node.event);
            for (var propertyName in node.event) {
                console.log(propertyName + " = " + node.event[propertyName]);
            }

            authorize(node.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: node.authConfig.credential_type,
                    PrimaryIdentity: node.authConfig.username
                }
            }, onAuthorized, error);
        });

        function onAuthorized(credentials) {
            node.status({fill: "green", shape: "dot", text: "connected"});
            const event = node.event;//node.events[i];
            if (event.TypeId) {
                event.Id = event.TypeId;
                delete event.Name;
                delete event.TypeId;
                // data.push({
                //     'tns:request': {
                //         EventData: {
                //             AuthenticationToken: credentials.token,
                //             Device: {EntityId: credentials.device_id},
                //             Event: event,//{Id: data.event.event_id, Name: data.event.Name, ParameterList: {Parameter: []}},
                //             TimeStamp: node.timestamp.toString()
                //         }
                //     }
                // });
            } else {
                node.status({fill: "red", shape: "ring", text: "Unrecognized event type (no TypeId): " + event.Id});
            }
            sendEvent(node.url + DEVICE_SERVICE_SOAP_PATH, {
                //'tns:request': {
                request: {
                    EventData: {
                        AuthenticationToken: credentials.token,
                        Device: {EntityId: credentials.device_id},
                        Event: event,
                        TimeStamp: new Date().toISOString()
                    }
                }
            }, function () {
                node.status({});
            }, error);
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Event out", HeliosEventOutNode);
};