var signalR = require('signalr-client');

const TAG = '[helios-signalr]';

module.exports = function (RED) {
    function HeliosSignalR(config) {
        RED.nodes.createNode(this, config);

        this.client = null;

        var node = this;

        function createClient(config) {
            var client  = new signalR.client(config.url, ['EventsHub'], 10, true /* doNotStart is option and defaulted to false. If set to true client will not start until .start() is called */
            );

            client.serviceHandlers = {
                bound: function() {
                    var message = "Websocket bound";
                    node.status({fill: "green", shape: "ring", text: message});
                    console.log(message);
                },
                connectFailed: function(error) {
                    var message = "Websocket connectFailed: " + error;
                    node.status({fill: "red", shape: "ring", text:message});
                    node.error(message);
                },
                connected: function(connection) {
                    var message = "Websocket connected";
                    node.status({fill: "green", shape: "ring", text:message});
                    console.log(message);
                },
                disconnected: function() {
                    var message = "Websocket disconnected";
                    node.status({fill: "orange", shape: "ring", text:message});
                    console.log(message);
                },
                onerror: function (error) {
                    var message = "Websocket onerror: " + error;
                    node.status({fill: "red", shape: "ring", text:message});
                    node.error(message);
                },
                messageReceived: function (message) {
                    console.log(message);
                    return false;
                },
                bindingError: function (error) {
                    var message = "Websocket bindingError: " + error;
                    node.status({fill: "red", shape: "ring", text:message});
                    node.error(message);
                },
                connectionLost: function (error) {
                    var message = "Connection Lost: " + error;
                    node.status({fill: "orange", shape: "ring", text:message});
                    console.log(message);
                },
                reconnecting: function (retry /* { inital: true/false, count: 0} */) {
                    var message = "Websocket Retrying: " + retry;
                    node.status({fill: "orange", shape: "ring", text:message});
                    console.log(message);
                    //return retry.count >= 3; /* cancel retry true */
                    return true;
                }
            };
            return client;
        }

        this.on('input', function (msg) {
            node.status({});
            var data = {};
            if (config.data_source == 'payload') {
                // Sample:
                // {
                //     "url": "",
                //     "hub": "",
                //     "method": ""
                // }
                if (msg.payload) {
                    data = msg.payload;
                } else {
                    return;
                }
            } else {
                data = {
                    url: config.url,
                    hub: config.hubname,
                    method: config.methodname
                };
            }

            console.log(TAG + " Payload: " + JSON.stringify(data));
            if(node.client) {
                node.client.end();
            }
            node.client = createClient(data);
            var h = {};
            h[data.method] = function(e) {
                node.send({payload: e});
            };
            node.client.handlers[data.hub] = h;
            node.client.start();
        });

        this.on("close",function () {
            if(node.client) {
                node.client.end();
            }
        });
    }

    RED.nodes.registerType("HeliosSignalR", HeliosSignalR);
};
