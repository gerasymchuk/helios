const soap = require('soap');
const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

const WSDL = require('soap/lib/wsdl').WSDL;
const orig = WSDL.prototype.objectToXML;
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]);
    if(xml.indexOf("SendAlarm") > -1) {
        xml = orderAlphabetically(xml, 'alarm');
    }
    return xml;
};

function orderAlphabetically(xml, nodeName) {
    var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
    var p = d.getElementsByTagName(nodeName);
    Array.prototype.slice.call(p).forEach(function (element, index, array) {
        Array.prototype.slice.call(element.childNodes)
            .sort(function (x, y) {
                if(x.nodeName < y.nodeName) return -1;
                if(x.nodeName > y.nodeName) return 1;
                return 0;
            }).forEach(function (x) { element.appendChild(x); });
    });
    return xmlserializer.serializeToString(d);
}

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const ALARM_SERVICE_SOAP_PATH = '/AlarmService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const SEND_ALARM_ACTION = 'http://tempuri.org/IAlarmService/SendAlarm';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-send-alarm]';

function authorize(url, args, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
                client.Authorize(args, function(err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                        resolve({
                            token: result.AuthorizeResult.AuthenticationToken,
                            device_id: result.AuthorizeResult.EntityId
                        });
                    }
                });
            }
        }, url);
}

function sendAlarm(url, args, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client){
                client.on('request', function(xmlMsg) {
                    console.log("Send alarm XML: " + xmlMsg);
                });
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SEND_ALARM_ACTION);
                client.addSoapHeader('');
                client.SendAlarm(args, function(err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else {
                        resolve(result);
                    }
                });
            }


        }, url);
}


module.exports = function (RED) {
    function HeliosSendAlarm(config) {
        RED.nodes.createNode(this, config);

        const node = this;

        const authConfig = RED.nodes.getNode(config.auth);

        this.on('input', function(msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});

            if (config.data_source == 'payload') {
                console.log("Input: ", msg);
                if (msg.payload && msg.payload.Alarm) {
                    node.alarm = msg.payload.Alarm;
                } else {
                    return;
                }
            } else {
                node.alarm = {ErrorCode: config.error_code};
                if(config.status_id) {
                    node.alarm.StatusID = config.status_id;
                    node.alarm.AlarmStatus = config.status_id;
                }
                if(config.message) {
                    node.alarm.Message = config.message;
                }
                if(config.priority_id) {
                    node.alarm.PriorityId = config.priority_id;
                }
            }

            node.alarm.TimeStamp = new Date().toISOString();

            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error);
        });

        function onAuthorized(credentials){
            node.status({fill:"green",shape:"dot",text:"connected"});
            if(!node.alarm.EntityID) {
                node.alarm.EntityID = credentials.device_id;
            }

            sendAlarm(config.url + ALARM_SERVICE_SOAP_PATH, {
                authenticationToken: credentials.token,
                alarm: node.alarm
            }, function(result){
                console.log(TAG, 'result', result);
                if (result) {
                    node.send({payload: result})
                }
            }, error);
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Alarm", HeliosSendAlarm);
};
