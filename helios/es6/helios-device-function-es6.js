const soap = require('soap');

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const DEVICE_SERVICE_SOAP_PATH = '/DeviceService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const GET_PENDING_FUNCTION_ACTION = 'http://tempuri.org/IDeviceService/GetPendingFunctions';
const FUNCTION_COMPLETED_ACTION = 'http://tempuri.org/IDeviceService/FunctionCompleted';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-device-function]';

function authorize(url, args) {
    return new Promise(function(resolve, reject){
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
            if (err) {
                reject(new Error("Service Call Error: " + err));
            }
            
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
            client.Authorize(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                }
                if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                    resolve({
                        token: result.AuthorizeResult.AuthenticationToken,
                        device_id: result.AuthorizeResult.EntityId
                    });
                }
            });
        }, url);
    });
}

function getPendingFunctions(url, args) {
    return new Promise(function(resolve, reject){
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            }

            // client.on('request', function(xmlMsg) {
            //     console.log("getPendingFunctions XML: " + xmlMsg);
            // });

            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_PENDING_FUNCTION_ACTION);
            client.GetPendingFunctions(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    if(result) {
                        result.token = args.authenticationToken
                    }
                    resolve(result);
                }
            });
        }, url);
    });
}

function functionCompleted(url, args) {
    return new Promise(function(resolve, reject){
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            }

            // client.on('request', function(xmlMsg) {
            //     console.log("FunctionCompleted XML: " + xmlMsg);
            // });

            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, FUNCTION_COMPLETED_ACTION);
            client.FunctionCompleted(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }, url);
    });
}

module.exports = function (RED) {
    function HeliosFunction(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var authConfig = RED.nodes.getNode(config.auth);

        setInterval(function(){
            node.emit("input", {});
        }, config.repeat_interval * 1000);

        this.on('input', function(msg) {
            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            })
                .then(function(credentials) {
                    return getPendingFunctions(config.url + DEVICE_SERVICE_SOAP_PATH, {
                        authenticationToken: credentials.token,
                        deviceFilterList: {
                            DeviceFilterCriteria: {
                                EntityId: credentials.device_id
                            }
                        }
                    });
                })
                .then(function(result){
                    console.log(TAG, 'result', result);
                    if (result) {
                        node.send({payload: result.GetPendingFunctionsResult});
                        if (result.GetPendingFunctionsResult && result.GetPendingFunctionsResult.Function && result.GetPendingFunctionsResult.Function.length) {
                            var p = [];
                            for (var i = 0; i < result.GetPendingFunctionsResult.Function.length; i++) {
                                p.push(functionCompleted(config.url + DEVICE_SERVICE_SOAP_PATH, {
                                    authenticationToken: result.token,
                                    functionID: result.GetPendingFunctionsResult.Function[i].Id
                                }))
                            }
                            return Promise.all(p);
                        }
                    }
                })
                .then(function(result) {
                    console.log(TAG, 'mark functions completed result', result)
                })
                .catch(function(err) {
                    node.status({fill: "red", shape: "ring", text: err.message});
                    node.error(err.message);
                });

        });
    }

    RED.nodes.registerType("helios-device-function", HeliosFunction);
};
