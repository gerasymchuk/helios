[![Go to Gunnebo](logo.png)](http://gunnebo.com)

# node-red-contrib-liteputer
Node-RED node that composes input for [node-red-contrib-artnet](https://github.com/gunnebo-ab/node-red-contrib-cameo) to control [Lite-Puter](http://www.liteputer.com.tw) Dimmer Pack.

[Gunnebo](http://www.gunnebo.com/)  (OMX: GUNN) is a multinational corporation headquartered in Gothenburg, Sweden, specializing in security products, services and solutions mainly in the areas of cash management, entrance security, electronic security and safes & vaults. The Gunnebo Group has operations in 32 countries with approximately 5,500 employees (Jan 2016) and a reported global revenue of €660 million for 2015. Gunnebo has been listed on the Stockholm Stock Exchange in Sweden since 1994 and can be found on the NASDAQ OMX Nordic Exchange Stockholm in the Mid Cap Industrials segment.

## Install

Run the following command in the root directory of your Node-RED install. Usually this is `~/.node-red`
```
npm install node-red-contrib-liteputer
```

## Using

You can either setup node via editor or with a payload like the following example:

```
msg.payload = {
    address_start:1,
    channel1:0,
    channel2:0,
    channel3:0,
    channel4:0,
    mode:0,
    chaser_speed: 0,
    master: 0
};

```

While configuring node via editor you can specify "-1" as a value for any channel. In that case channel value doesn't change.