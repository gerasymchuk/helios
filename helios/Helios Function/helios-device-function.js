const soap = require('soap');

const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

const WSDL = require('soap/lib/wsdl').WSDL;
const orig = WSDL.prototype.objectToXML;
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]).replace('xsi:nil="true"', '');
    if (xml.indexOf("SendFunction") > -1) {
        xml = orderAlphabetically(xml, "function");
    }
    return xml;
};

function orderAlphabetically(xml, nodeName) {
    var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
    var p = d.getElementsByTagName(nodeName);
    Array.prototype.slice.call(p).forEach(function (element, index, array) {
        Array.prototype.slice.call(element.childNodes)
            .sort(function (x, y) {
                if (
                    x.nodeName < y.nodeName) return -1;
                if (x.nodeName > y.nodeName) return 1;
                return 0;
            })
            .forEach(function (x) {
                element.appendChild(x);
            });
    });
    return xmlserializer.serializeToString(d);
}

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const DEVICE_SERVICE_SOAP_PATH = '/DeviceService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const GET_PENDING_FUNCTION_ACTION = 'http://tempuri.org/IDeviceService/GetPendingFunctions';
const GET_PENDING_FUNCTION_WITH_CHILDREN_ACTION = 'http://tempuri.org/IDeviceService/GetPendingFunctionsWithChildren';
const FUNCTION_COMPLETED_ACTION = 'http://tempuri.org/IDeviceService/FunctionCompleted';
const SEND_FUNCTION_ACTION = 'http://tempuri.org/IDeviceService/SendFunction';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-function]';

var FUNCTION_NAMES = {
    '10': 'Configuration',
    '15': 'Execute  Script',
    '51': 'Operational Mode Change',
    '52': 'Reset',
    '53': 'Reconciliate',
    '200': 'Message',
    '301': 'Input Change',
    '302': 'Output Change',
    '500': 'Support',
    '900': 'Statistics',
    '60': 'Policy Change',
    '65': 'Mode Change',
    '66': 'Schedule Change',
    '1001': 'Entry',
    '1002': 'Cancelled Entry',
    '1004': 'Allow Entry',
    '1101': 'Temperature Change',
    '1201': 'Cash Deposit',
    '1202': 'Cash Withdrawal',
    '1203': 'Emptying',
    '1204': 'Refill',
    '1205': 'Balance Update',
    '1206': 'Cash Movement',
    '1301': 'Movement',
    '1302': 'Start Recording',
    '1303': 'Stop Recording',
    '1304': 'Activate Surveillance',
    '1305': 'Deactivate Surveillance',
    '1306': 'Retrieve Recording',
    '1307': 'Take Snapshot',
    '1308': 'Alarm',
    '1309': 'Cancel Alarm',
    '1400': 'New Attendee',
    '2501': 'Start Playback',
    '2502': 'Playback Started',
    '2503': 'Screen Overlay',
    '3001': 'Weather Change'
};

function authorize(url, args, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
                client.Authorize(args, function(err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                        resolve({
                            token: result.AuthorizeResult.AuthenticationToken,
                            device_id: result.AuthorizeResult.EntityId
                        });
                    }
                });
            }
        }, url);
}

function getPendingFunctions(url, args, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_PENDING_FUNCTION_ACTION);

                // client.on('request', function (xmlMsg) {
                //     console.log("Get Pending XML: " + xmlMsg);
                // });
                client.GetPendingFunctions(args, function(err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else {
                        if(result) {
                            result.token = args.authenticationToken
                        }
                        resolve(result);
                    }
                });
            }
        }, url);
}

function getPendingFunctionsWithChildren(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, GET_PENDING_FUNCTION_WITH_CHILDREN_ACTION);

            // client.on('request', function (xmlMsg) {
            //     console.log("Get Pending with children XML: " + xmlMsg);
            // });
            client.GetPendingFunctionsWithChildren(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    if(result) {
                        result.token = args.authenticationToken
                    }
                    resolve(result);
                }
            });
        }
    }, url);
}

function functionCompleted(url, func, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, FUNCTION_COMPLETED_ACTION);
                client.FunctionCompleted(func, function(err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else {
                        //resolve(url, functions, result);
                        resolve();
                    }
                });
            }
        }, url);
}

function sendFunction(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client){
            // client.on('request', function(xmlMsg) {
            //     console.log("Send function XML: " + xmlMsg);
            // });
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SEND_FUNCTION_ACTION);
            client.addSoapHeader('');
            client.SendFunction(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }
    }, url);
}

module.exports = function (RED) {
    function HeliosDeviceFunction(config) {
        RED.nodes.createNode(this, config);
        this.url = config.url;
        this.repeat_interval = config.repeat_interval;
        this.child_events = config.child_events;
        var node = this;
        this.interval_id = null;

        var authConfig = RED.nodes.getNode(config.auth);

        this.interval_id = setInterval(function(){
            node.emit("input", {});
        }, this.repeat_interval * 1000);

        this.on('input', function(msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});
            authorize(this.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error)
        });

        function onAuthorized(credentials) {
            node.status({fill:"green",shape:"dot",text:"connected"});
            var func = node.child_events ? getPendingFunctionsWithChildren : getPendingFunctions;
            var data = node.child_events ?
            {
                authenticationToken: credentials.token,
                entityId: credentials.device_id
            } :
            {
                authenticationToken: credentials.token,
                deviceFilterList: {
                    DeviceFilterCriteria: {
                        EntityId: credentials.device_id
                    }
                }
            };
            var handler = node.child_events ? onGetPendingFunctionsWithChildren : onGetPendingFunctions;
            func(node.url + DEVICE_SERVICE_SOAP_PATH, data, handler, error);
        }

        function onGetPendingFunctionsWithChildren(result){
            //console.log(TAG, 'onGetPendingFunctionsWithChildren result', result);
            if (result
                && result.GetPendingFunctionsWithChildrenResult
                && result.GetPendingFunctionsWithChildrenResult.Function
                && result.GetPendingFunctionsWithChildrenResult.Function.length) {
                result.GetPendingFunctionsWithChildrenResult.Function.map(function(obj){
                    return {token: result.token, data: obj};
                }).forEach(processPendingFunction);
            }
        }

        function onGetPendingFunctions(result){
            //console.log(TAG, 'onGetPendingFunctions result', result);
            if (result && result.GetPendingFunctionsResult && result.GetPendingFunctionsResult.Function && result.GetPendingFunctionsResult.Function.length) {
                    result.GetPendingFunctionsResult.Function.map(function(obj){
                        return {token: result.token, data: obj};
                    }).forEach(processPendingFunction);
            }
        }

        function processPendingFunction(element, index, array) {
            functionCompleted(node.url + DEVICE_SERVICE_SOAP_PATH, {
                authenticationToken: element.token,
                functionID: element.data.Id
            }, function() {
                node.send({payload: element.data})
            }, error);
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Function in", HeliosDeviceFunction);

    HeliosDeviceFunction.prototype.close = function() {
        if (this.interval_id != null) {
            clearInterval(this.interval_id);
        }
    };

    function HeliosSendFunction(config) {
        RED.nodes.createNode(this, config);
        this.data_source = config.data_source;
        this.function_type = config.function_type;

        var node = this;

        var authConfig = RED.nodes.getNode(config.auth);

        this.on('input', function(msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});

            if (node.data_source == 'payload') {
                // Sample:
                //({ "Function": [
                // { "Id": 6922, "Name": "Output Change", "ParameterList":
                //                                          { "Parameter": [
                //                                              { "Name": "Power State", "Type": { "Id": 2, "Name": "String", "Required": false } },
                //                                              { "Name": "Level", "Type": { "Id": 2, "Name": "String", "Required": false }, "Value": "0.6" }
                //                                          ] },
                // "ReferenceId": 0, "TypeId": 302 },
                // ....]}).
                if (msg.payload) {
                    node.function = msg.payload;
                } else {
                    return;
                }
            } else {
                var param_list = {Parameter: []};
                for (var i = 0; i < config.function_params.length; i++) {
                    var param = config.function_params[i];
                    param_list.Parameter.push({
                        Details: '',
                        Name: param.Name,
                        Type: {Id: param.Id},
                        Value: param.Value
                    });
                }
                node.function = {
                    Name: FUNCTION_NAMES[node.function_type.toString()],
                    ParameterList: param_list,
                    TypeId: node.function_type
                };
            }


            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error);
        });

        function onAuthorized(credentials){
            node.status({fill:"green",shape:"dot",text:"connected"});
            node.function.EntityID = credentials.device_id;
            sendFunction(config.url + DEVICE_SERVICE_SOAP_PATH, {
                authenticationToken: credentials.token,
                function: node.function
            }, function(result){
                //console.log(TAG, 'result', result);
                if (result) {
                    node.send({payload: result})
                }
            }, error);
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Function out", HeliosSendFunction);
};
