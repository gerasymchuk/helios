module.exports = function (RED) {
    function AuthConfig(n) {
        RED.nodes.createNode(this, n);
        this.username = n.username;
        this.password = n.password;
		this.credential_type = n.credential_type;
    }
    RED.nodes.registerType("auth-config", AuthConfig);
}