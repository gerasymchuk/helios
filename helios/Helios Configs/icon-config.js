module.exports = function (RED) {
    function IconConfig(n) {
        RED.nodes.createNode(this, n);
        this.name = n.name;
    }
    RED.nodes.registerType("icon-config", IconConfig);
}