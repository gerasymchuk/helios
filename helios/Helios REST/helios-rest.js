const request = require('request');
const TAG = '[helios-rest]';

module.exports = function (RED) {
    function HeliosRest(config) {
        RED.nodes.createNode(this, config);

        const node = this;

        this.on('input', function (msg) {
            node.status({});
            var data = {};
            if (config.data_source == 'payload') {
                // Sample:
                // {
                //     "Url": "",
                //     "Endpoint": "",
                //     "Data": "{}",
                // }
                if (msg.payload) {
                    data = msg.payload;
                    data.Data = JSON.stringify(data.Data);
                } else {
                    return;
                }
            } else {
                data = {
                    Url: config.url,
                    Endpoint: config.endpoint,
                    Data: config.data
                };
            }
            console.log(TAG + " Payload: " + JSON.stringify(data));
            const url = data.Url + "/" + data.Endpoint;
            console.log(TAG + " URL: " + url);
            console.log(TAG + " DATA: " + data.Data);
            request({
                uri: url,
                method: 'POST',
                header:{
                    "Content-Type": "application/json"
                },
                body: data.Data
            }, function(error, response, body){
                if (!error && response.statusCode == 200) {
                    console.log(TAG + "response: \n" + body);
                    node.status({fill:"green",shape:"dot",text:"send"});
                    node.send({payload: body});
                } else if (error) {
                    node.status({fill: "red", shape: "ring", text: error});
                    node.error(error);
                }
            });
        });
    }

    RED.nodes.registerType("Rest", HeliosRest);
};
