const request = require('request');
const builder = require('xmlbuilder');
const TAG = '[helios-sms-maingate]';

module.exports = function (RED) {
    function HeliosSMSMaingate(config) {
        RED.nodes.createNode(this, config);

        const node = this;

        this.on('input', function (msg) {
            node.status({});
            var data = {};
            if (config.data_source == 'payload') {
                // Sample:
                // {
                //     "Endpoint": "",
                //     "Username": "",
                //     "Password": "",
                //     "Number": "",
                //     "Message": "",
                // }
                if (msg.payload) {
                    data = msg.payload;
                } else {
                    return;
                }
            } else {
                data = {
                    Endpoint: config.url,
                    Username: config.username,
                    Password: config.password,
                    Number: config.number,
                    Message: config.message
                };
            }
            console.log(TAG + " Payload: " + JSON.stringify(data));
            const url = 'https://' + data.Username + ":" + data.Password + "@" + data.Endpoint;
            console.log(TAG + " URL: " + url);
            request(
                {
                    method: 'GET',
                    uri: url,
                    qs: { destAddr:data.Number, message:data.Message }
                }
                , function (error, response, body) {
                    console.log("Response code: " + response.statusCode);
                    if (!error && response.statusCode == 200) {
                        console.log(TAG + "response: \n" + body);
                        node.status({fill:"green",shape:"dot",text:"send"});
                        node.send({payload: body});
                    } else if (error) {
                        node.status({fill: "red", shape: "ring", text: error});
                        node.error(error);
                    }
                });
        });
    }

    RED.nodes.registerType("SMSMaingate", HeliosSMSMaingate);
};
