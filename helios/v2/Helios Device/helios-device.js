const soap = require('soap');

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const DEVICE_SERVICE_SOAP_PATH = '/DeviceService.svc';
const POLICY_SOAP_PATH = '/PolicyService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const SEND_FUNCTION_ACTION = 'http://tempuri.org/IDeviceService/SendFunction';
const SET_POLICY_ACTION = 'http://tempuri.org/IPolicyService/SetPolicy';
const SEND_EVENT_ACTION = 'http://tempuri.org/IDeviceService/SendEvent';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-device]';

const DOMParser = require('xmldom').DOMParser;
const xmlserializer = require('xmlserializer');

const WSDL = require('soap/lib/wsdl').WSDL;
const orig = WSDL.prototype.objectToXML;
WSDL.prototype.objectToXML = function (obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext) {
    var xml = orig.apply(this, [obj, name, nsPrefix, nsURI, isFirst, xmlnsAttr, schemaObject, nsContext]).replace('xsi:nil="true"', '');
    if(xml.indexOf("SendEvent") > -1) {
        xml = orderAlphabetically(xml, "q1:Parameter");
    }
    if(xml.indexOf("SetPolicy") > -1) {
        xml = orderAlphabetically(xml, 'policy');
    }
    return xml;
};

function orderAlphabetically(xml, nodeName) {
    var d = new DOMParser().parseFromString(xml, 'text/xml').firstChild;
    var p = d.getElementsByTagName(nodeName);
    Array.prototype.slice.call(p).forEach(function (element, index, array) {
        Array.prototype.slice.call(element.childNodes)
            .sort(function (x, y) {
                if(x.nodeName < y.nodeName) return -1;
                if(x.nodeName > y.nodeName) return 1;
                return 0;
            }).forEach(function (x) { element.appendChild(x); });
    });
    return xmlserializer.serializeToString(d);
}

function authorize(url, args, resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
            if (err) {
                reject(new Error("Service Call Error: " + err));
            } else if(client) {
                client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
                client.Authorize(args, function(err, result) {
                    if (err) {
                        reject(new Error("Service Call Error: " + err));
                    } else if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                        resolve({
                            token: result.AuthorizeResult.AuthenticationToken,
                            device_id: result.AuthorizeResult.EntityId
                        });
                    }
                });
            }
        }, url);
}

function setPolicy(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client) {
            // client.on('request', function (xmlMsg) {
            //     console.log("setPolicy XML: " + xmlMsg);
            // });
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SET_POLICY_ACTION);
            client.SetPolicy(args.shift(), function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }
    }, url);
}

function sendFunction(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if(client){
            // client.on('request', function(xmlMsg) {
            //     console.log("Send function XML: " + xmlMsg);
            // });
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SEND_FUNCTION_ACTION);
            client.addSoapHeader('');
            client.SendFunction(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }


    }, url);
}

function sendEvent(url, args, resolve, reject) {
    soap.createClient(url + WSDL_SUFFIX, {}, function (err, client) {
        if (err) {
            reject(new Error("Service Call Error: " + err));
        } else if (client) {
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SEND_EVENT_ACTION);

            client.on('request', function (xmlMsg) {
                console.log("Event XML: " + xmlMsg);
            });

            client.SendEvent(args, function (err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }
    }, url);
}

module.exports = function (RED) {
    function HeliosDeviceInNode(config) {
        RED.nodes.createNode(this, config);
        this.repeat_interval = config.repeat_interval;
        this.url = config.url;
        var node = this;
        this.interval_id = null;

        this.interval_id = setInterval(function(){
            node.emit("input", {});
        }, this.repeat_interval * 1000);

        var authConfig = RED.nodes.getNode(config.auth);

        this.on('input', function(msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});
            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error);
        });

        function onAuthorized(credentials) {
            //credentials.url, credentials.token, credentials.device_id
            node.status({fill: "green", shape: "dot", text: "connected"});
            credentials.url = node.url;
            node.send({payload: credentials})
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Device in", HeliosDeviceInNode);

    HeliosDeviceInNode.prototype.close = function() {
        if (this.interval_id != null) {
            clearInterval(this.interval_id);
        }
    };

    function HeliosDeviceOutNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var authConfig = RED.nodes.getNode(config.auth);

        this.on('input', function(msg) {
            if(!authConfig) {
                node.status({fill: "red", shape: "ring", text: "auth config is corrupted"});
                return;
            }
            node.status({});

            if (msg.payload) {
                node.payload = msg.payload;
            } else {
                return;
            }

            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            }, onAuthorized, error);
        });

        function onAuthorized(credentials){
            node.status({fill:"green",shape:"dot",text:"connected"});

            if(node.payload.hasOwnProperty("functions")) {
                node.payload.functions.forEach(function (element, index, array) {
                    element.EntityID = credentials.device_id;
                    sendFunction(config.url + DEVICE_SERVICE_SOAP_PATH, {
                        authenticationToken: credentials.token,
                        function: element
                    }, result, error);
                });
            } else if(node.payload.hasOwnProperty("policies")) {
                node.payload.policies.forEach(function (element, index, array) {
                    setPolicy(config.url + POLICY_SOAP_PATH, {
                        authenticationToken: credentials.token,
                        entityId: credentials.device_id,
                        policy: element
                    }, result, error);
                });
            } else if(node.payload.hasOwnProperty("events")) {
                node.payload.events.forEach(function (element, index, array) {
                    sendEvent(node.url + DEVICE_SERVICE_SOAP_PATH, {
                        'tns:request': {
                            EventData: {
                                AuthenticationToken: credentials.token,
                                Device: {EntityId: credentials.device_id},
                                Event: element,
                                TimeStamp: new Date().toISOString()
                            }
                        }
                    }, result, error);
                });
            }
        }

        function result(err) {
            node.status({fill: "green", shape: "ring"});
            console.log(TAG, 'result', result);
        }

        function error(err) {
            node.status({fill: "red", shape: "ring", text: err.message});
            node.error(err.message);
        }
    }

    RED.nodes.registerType("Device out", HeliosDeviceOutNode);
};
