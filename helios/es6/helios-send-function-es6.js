const soap = require('soap');

const AUTHORIZE_SOAP_PATH = '/SecurityService.svc';
const DEVICE_SERVICE_SOAP_PATH = '/DeviceService.svc';
const AUTHORIZE_ACTION = 'http://tempuri.org/ISecurityService/Authorize';
const SEND_FUNCTION_ACTION = 'http://tempuri.org/IDeviceService/SendFunction';
const WSDL_SUFFIX = '?singleWsdl';
const SOAP_ACTION_HEADER_NAME = 'SOAPAction';

const TAG = '[helios-send-function]';

var FUNCTION_NAMES = {
    '10': 'Configuration',
    '15': 'Execute  Script',
    '51': 'Operational Mode Change',
    '52': 'Reset',
    '53': 'Reconciliate',
    '500': 'Support',
    '900': 'Statistics',
    '1001': 'Entry',
    '1002': 'Cancelled Entry',
    '1004': 'Allow Entry',
    '1101': 'Temperature Change',
    '1201': 'Cash Deposit',
    '1202': 'Cash Withdrawal',
    '1203': 'Emptying',
    '1204': 'Refill',
    '1205': 'Balance Update',
    '1206': 'Cash Movement',
    '1301': 'Movement',
    '1302': 'Start Recording',
    '1303': 'Stop Recording',
    '1304': 'Activate  Surveillance',
    '1305': 'Deactivate Surveillance',
    '1306': 'Retrieve Recording',
    '1307': 'Take Snapshot',
    '1308': 'Alarm',
    '1309': 'Cancel Alarm',
    '2501': 'Start Playback',
    '2502': 'Playback Started',
    '2503': 'Screen Overlay',
    '3001': 'Weather Change'
};

function authorize(url, args) {
    return new Promise(function(resolve, reject){
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client) {
            if (err) {
                reject(new Error("Service Call Error: " + err));
            }
            // client.on('request', function (xmlMsg) {
            //     console.log("XML: " + xmlMsg);
            // });

            // node.status({fill: "green", shape: "dot", text: "SOAP Request..."});
            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, AUTHORIZE_ACTION);
            client.Authorize(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                }
                console.log("Result authenticate: " + result);
                if (result && result.AuthorizeResult && result.AuthorizeResult.AuthenticationToken) {
                    resolve({
                        token: result.AuthorizeResult.AuthenticationToken,
                        device_id: result.AuthorizeResult.EntityId
                    });
                }
            });
        }, url);
    });
}

function sendFunction(url, args) {
    return new Promise(function(resolve, reject) {
        soap.createClient(url + WSDL_SUFFIX, {}, function(err, client){
            if (err) {
                reject(new Error("Service Call Error: " + err));
            }

            // client.on('request', function(xmlMsg) {
            //     console.log("Send function XML: " + xmlMsg);
            // });

            client.addHttpHeader(SOAP_ACTION_HEADER_NAME, SEND_FUNCTION_ACTION);
            client.addSoapHeader('');
            client.SendFunction(args, function(err, result) {
                if (err) {
                    reject(new Error("Service Call Error: " + err));
                } else {
                    resolve(result);
                }
            });
        }, url);
    });
}


module.exports = function (RED) {
    function HeliosSendFunction(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var authConfig = RED.nodes.getNode(config.auth);

        this.on('input', function(msg) {
            authorize(config.url + AUTHORIZE_SOAP_PATH, {
                request: {
                    CredentialType: authConfig.credential_type,
                    PrimaryIdentity: authConfig.username
                }
            })
                .then(function(credentials){
                    var param_list = {Parameter: []};
                    for (var i = 0; i < config.function_params.length; i++) {
                        var param = config.function_params[i];
                        param_list.Parameter.push({
                            Details: '',
                            Name: param.Name,
                            Type: {Id: param.Id},
                            Value: param.Value
                        });
                    }

                    return sendFunction(config.url + DEVICE_SERVICE_SOAP_PATH, {
                        authenticationToken: credentials.token,
                        function: {
                            EntityID: credentials.device_id,
                            Name: FUNCTION_NAMES[config.function_type.toString()],
                            ParameterList: param_list,
                            TypeId: config.function_type
                        }
                    })
                })
                .then(function(result){
                    console.log(TAG, 'result', result);
                    if (result) {
                        node.send({payload: result})
                    }
                })
                .catch(function(err) {
                    node.status({fill: "red", shape: "ring", text: err.message});
                    node.error(err.message);
                });

        });
    }

    RED.nodes.registerType("helios-send-function", HeliosSendFunction);
};
