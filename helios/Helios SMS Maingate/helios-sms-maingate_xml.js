const request = require('request');
const builder = require('xmlbuilder');
//const dateFormat = require('dateformat');
const TAG = '[helios-sms-maingate]';

module.exports = function (RED) {
    function HeliosSMSMaingate(config) {
        RED.nodes.createNode(this, config);

        const node = this;


        this.on('input', function (msg) {
            node.status({});
            var data = {};
            if (config.data_source == 'payload') {
                // Sample:
                // {
                //     "Endpoint": "",
                //     "Company": "",
                //     "Username": "",
                //     "Password": "",
                //     "Number": "",
                //     "Message": "",
                // }
                if (msg.payload) {
                    data = msg.payload;
                } else {
                    return;
                }
            } else {
                data = {
                    Endpoint: config.url,
                    Company: config.company,
                    Username: config.username,
                    Password: config.password,
                    Number: config.number,
                    Message: config.message
                };
            }
            console.log("Payload: " + JSON.stringify(data));
            // const sendSMSUrl = data.Endpoint + '/send/xml/' + "?id=" + data.Company.replace(/ /g, "+") + "&user=" + data.Username + "&pass=" + data.Password;
            // console.log(TAG + " url: " + sendSMSUrl);

            // login(data, function(body) {
            //     bind(data, function(body) {
            //         sendSMS(data, function(body) {
            //             unbind(data, function (body) {
            //                 logout(data, function (body) {
            //                     node.status({fill:"green",shape:"dot",text:"send"});
            //                 }, onError)
            //             }, onError);
            //         }, onError);
            //     }, onError);
            // }, onError);
            const url = 'https://' + data.Username + ":" + data.Password + "@" + data.Endpoint;
            console.log("URL: " + url);
            request(
                {
                    method: 'GET',
                    uri: url,
                    qs: { destAddr:data.Number, message:data.Message }
                }
                , function (error, response, body) {
                    console.log("Response code: " + response.statusCode);
                    if (!error && response.statusCode == 200) {
                        console.log(TAG + "response: \n" + body);
                        node.status({fill:"green",shape:"dot",text:"send"});
                    } else if (error) {
                        onError(error);
                    }
                });
        });

        function login(data, successHandler, errorHandler) {
            // <Authenticate>
            //     <Login>
            //         <ID Type="Name"></ID>
            //         <Password></Password>
            //     </Login>
            // </Authenticate>
            const body = builder.create({
                Authenticate: {
                    Login: {
                        ID: { '#text': data.Username, '@Type': 'Name'},
                        Password: data.Password
                    }
                }
            }).end({ pretty: true});
            server_request(data.Endpoint, body, successHandler, errorHandler);
        }

        function logout(data, successHandler, errorHandler) {
            // <Authenticate>
            //     <Logout/>
            // </Authenticate>
            const body = builder.create({Logout: ''}).end({ pretty: true});
            server_request(data.Endpoint, body, successHandler, errorHandler);
        }

        function bind(data, successHandler, errorHandler) {
            // <Service>
            //     <Bind>
            //         <Name>Message</Name>
            //     </Bind>
            // </Service>
            const body = builder.create({
                Service: {
                    Bind: {
                        Name: 'Message'
                    }
                }
            }).end({ pretty: true});
            server_request(data.Endpoint, body, successHandler, errorHandler);
        }

        function unbind(data, successHandler, errorHandler) {
            // <Service>
            //     <Release>
            //         <ServiceID>11111</ServiceID>
            //     </Release>
            // </Service>
            const body = builder.create({
                Service: {
                    Release: {
                        ServiceID: data.ServiceID
                    }
                }
            }).end({ pretty: true});
            server_request(data.Endpoint, body, successHandler, errorHandler);
        }

        function sendSMS(data, successHandler, errorHandler) {
            // <Operation ServiceID="">
            //      <Message_v1.2>
            //          <send>
            //              <Target Type="MGNo">12034</Traget>
            //              <Content Type="Alpha">This is a message</Content>
            //              <Subject></Subject>
            //          </send>
            //      </Message_v1.2>
            //  </Operation>
            const body = builder.create({
                Operation: {
                    'Message_v1.2': {
                        send: {
                            Target: {'#text': data.Number, '@Type': 'MGNo'},
                            Content: {'#text': data.Message, '@Type': 'Alpha'},
                            Subject: data.Company
                        }
                    },
                    '@ServiceID': data.ServiceID
                }
            }).end({ pretty: true});
            server_request(data.Endpoint, body, successHandler, errorHandler);
        }

        function server_request(url, body, successHandler, errorHandler) {
            console.log(TAG + "body: \n" + body);
            request(
                {
                    method: 'POST',
                    uri: 'https://' + url,
                    headers: {
                        'Content-Type': 'text/xml'
                    },
                    body: body.toString()
                }
                , function (error, response, body) {
                    console.log("Response code: " + response.statusCode);
                    if (!error && response.statusCode == 200) {
                        console.log(TAG + "response: \n" + body);
                        successHandler(body);
                    } else if (error) {
                        errorHandler(error);
                    }
                });
        }

        function error(error) {
            node.status({fill: "red", shape: "ring", text: error});
            node.error(error);
        }
    }

    RED.nodes.registerType("SMSMaingate", HeliosSMSMaingate);
};
